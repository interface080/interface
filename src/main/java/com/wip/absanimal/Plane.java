/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public class Plane extends Vehicle implements Flyable, Runnable {

    private String name;

    public Plane(String name) {
        super("Plane");
        this.name = name;
    }

    @Override
    public void startMachine() {
        System.out.println("Plane: " + this.name + " Start Machine.");
    }

    @Override
    public void stopMachine() {
        System.out.println("Plane: " + this.name + " Stop Machine.");
    }

    @Override
    public void increaseSpeed() {
        System.out.println("Plane: " + this.name + " increase speed.");
    }

    @Override
    public void reduceSpeed() {
        System.out.println("Plane: " + this.name + " reduce speed.");
    }

    @Override
    public void brake() {
        System.out.println("Plane: " + this.name + " brake.");
    }

    @Override
    public void fly() {
        System.out.println("Plane: " + this.name + " fly.");
    }

    @Override
    public void run() {
        System.out.println("Plane: " + this.name + " run.");
    }
}
