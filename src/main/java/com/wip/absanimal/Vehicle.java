/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public abstract class Vehicle {

    private String name;

    public Vehicle(String name) {
        this.name = name;
    }

    public abstract void startMachine();

    public abstract void stopMachine();

    public abstract void increaseSpeed();

    public abstract void reduceSpeed();

    public abstract void brake();
}
