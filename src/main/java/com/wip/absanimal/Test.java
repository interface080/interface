/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public class Test {

    public static void main(String[] args) {
        //Flyable
        //Bird
        Bird b = new Bird("Tohru");
        b.fly();
        b.sleep();

        //Plane
        Plane p = new Plane("R1");
        p.run();
        p.fly();
        p.increaseSpeed();

        System.out.println("");
        //Flyable Loop
        Flyable[] flyables = {b, p};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane plane = (Plane) f;
                plane.startMachine();
                plane.run();
            }

            f.fly();
        }
        System.out.println("");

        //Runnable
        //Wolf
        wolf w = new wolf("Ranga");
        w.run();
        w.eat();

        //Car
        Car c = new Car("Mustang");
        c.startMachine();
        c.run();
        c.brake();
        System.out.println("");
        //Runnable Loop
        Runnable[] runnables = {p, w, c};
        for (Runnable r : runnables) {
            if (r instanceof Car) {
                Car car = (Car) r;
                car.startMachine();
            }
            else if (r instanceof Plane) {
                Plane plane = (Plane) r;
                plane.startMachine();
            }

            r.run();
        }

    }
}
