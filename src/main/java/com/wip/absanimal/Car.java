/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public class Car extends Vehicle implements Runnable {
    private String name;

    public Car(String name) {
        super("Car");
        this.name = name;
    }

    @Override
    public void startMachine() {
        System.out.println("Car: " + name + " Start Machine.");
    }

    @Override
    public void stopMachine() {
        System.out.println("Car: " + name + " Stop Machine.");
    }

    @Override
    public void increaseSpeed() {
        System.out.println("Car: " + name + " increase speed.");
    }

    @Override
    public void reduceSpeed() {
        System.out.println("Car: " + name + " reduce speed.");
    }

    @Override
    public void brake() {
        System.out.println("Car: " + name + " brake.");
    }

    @Override
    public void run() {
        System.out.println("Car: " + name + " run.");
    }
    
}
