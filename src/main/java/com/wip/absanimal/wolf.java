/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absanimal;

/**
 *
 * @author WIP
 */
public class wolf extends LandAnm {
    private String nickname;

    public wolf(String nickname) {
        super("Wolf", 4);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Wolf: " + nickname + " eat.");
    }

    @Override
    public void walk() {
        System.out.println("Wolf: " + nickname + " walk.");
    }

    @Override
    public void speak() {
        System.out.println("Wolf: " + nickname + " speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Wolf: " + nickname + " sleep.");
    }

    @Override
    public void run() {
        System.out.println("Wolf: " + nickname + " run.");
    }
    
}
